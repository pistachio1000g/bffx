from common import define
from datetime import datetime
import pickle
import os
from os import path

# 反対側のポジション名を取得する
# 'BUY'が渡されたら'SELL'を返す
def getRevPos(side :str) -> str:
    if side == define._BUY:
        return define._SELL
    else:
        return define._BUY

# 渡された注文サイド文字列から注文サイドコードを返す
def getSideCode(side :str) -> int:
    if side == define._BUY:
        return define._BUY_NOW
    elif side == define._SELL:
        return define._SELL_NOW

    return 0
        
# 渡された注文サイドコードから注文サイド文字列を返す
def getSideString(sideCode :int) -> str:
    if sideCode == define._BUY_NOW:
        return define._BUY
    elif sideCode == define._SELL_NOW:
        return define._SELL

    return ''

# 渡された取引所の状態文字列から取引できる状態か返す
def getTradePropriety(helth :str) -> bool:
    try:
        # if (helth == define._NOMAL) or (helth == define._BUSY):
        if helth != define._SUPER_BUSY:
            return True
        else:
            return False
    except TypeError:
        # たまーにサーバが重くて受け取れない時があるのでそのときはFalse
        return False
                
# 渡されたVolumeから利確を行うべきか判定する
def checkConfirmVolume(side :str, buy :float, sell :float) -> bool:
    '''
    True:利確する
    False:利確しなくていい
    '''
    # ポジションなし
    if side == define._NONE:
        return False   

    # 発注後、volumeの値が既定値まで行っている場合は利確・ロスカットする
    if (side == define._BUY and sell - buy > define._CUT_VOLUME) or (side == define._SELL and buy - sell > define._CUT_VOLUME):
    # if (side == define._BUY and sell / buy > define._CUT_RATIO) or (side == define._SELL and buy / sell > define._CUT_RATIO):
    # if (side == define._SELL and sell / buy > define._CUT_RATIO) or (side == define._BUY and buy / sell > define._CUT_RATIO):
        return True

    return False   

# volumeから発注すべきか判定する
def checkOrderingVolume(side :str, buy :float, sell :float) -> bool:
    '''
    True:発注する
    False:発注しちゃダメ
    '''

    # buyとsellのvolに一定の差がついた時発注をかける
    try:
        if side == define._NONE and ((buy - sell) > define._VOLUME or (sell - buy) > define._VOLUME):
        # if side == define._NONE and ((buy / sell > define._RATIO) or (sell / buy > define._RATIO)):
            return True
    except ZeroDivisionError:
        return False

    return False

# 乖離率から発注すべきか判定する
def checkDeviation(code :int, deviation :float) -> bool:
    '''
    True:発注する
    False:発注しちゃダメ
    '''

    # 乖離率が一定値を超えている場合は発注しない
    if (code == define._BUY_NOW and deviation >= define._DEVIATION＿RATE) or (code == define._SELL_NOW and deviation <= define._DEVIATION＿RATE * -1.0):
        return False

    return True

# volumeから発注サイド文字列を取得する
def getSideStringByVolume(buy :float, sell :float) -> str:
    if (buy - sell) > define._VOLUME:
    # if (buy / sell > define._RATIO):
        # return define._SELL
        return define._BUY

    if (sell - buy) > define._VOLUME:
    # if (sell / buy > define._RATIO):
        # return define._BUY
        return define._SELL

    return define._NONE

# 時刻からメンテ時間に入ったか判定する
def checkMaintenanceTime() -> bool:
    hour    = int(datetime.now().strftime("%H"))
    minutes = int(datetime.now().strftime("%M"))

    # 3:50から4:20まではトレードしない
    if datetime(2015, 6, 27, hour, minutes, 0) >= datetime(2015, 6, 27, 3, 50, 0) and datetime(2015, 6, 27, hour, minutes, 0) <= datetime(2015, 6, 27, 4, 20, 0):
        return True
    
    return False

# pickleに書き込む
def writePickle(status):
    with open(path.dirname(path.abspath(__file__)) + '\\side.pickle', 'wb') as f:
        pickle.dump(status, f)

# pickleから読み込む
def loadPickle():
    with open(path.dirname(path.abspath(__file__)) + '\\side.pickle', 'rb') as f:
        status = pickle.load(f)

    return status