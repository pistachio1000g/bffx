import time
import pybitflyer
from common import define
from common import utility
from datetime import datetime
from requests.exceptions import ConnectionError

# pipflyerのうっすいラッパークラス
class bffFlyer(pybitflyer.API):
    def __init__(self, api_key=define._API_KEY, api_secret=define._API_SECRET, timeout=None):
        super().__init__(api_key, api_secret, timeout)

    def order(self, side :str, size=define._POS):
        """
        注文を出す
        :param side: BUY/SELLどちらかの文字列
        :param size: 注文サイズ。デフォ値はdefine._POS
        :return: 注文結果
        """        
        try:
            result = self.sendchildorder(product_code=define._FX_BTC_JPY, child_order_type=define._MARKET, side=side, size=size)
        except ConnectionError:
            return ''

        return result

    def ordering(self, side :str, size=define._POS):
        """
        注文を出す。基本はこちらを使う
        :param side: BUY/SELLどちらかの文字列
        :param size: 注文サイズ。デフォ値はdefine._POS
        :return: 注文結果
        """
        result = self.order(side, size)
        if not 'child_order_acceptance_id' in result:
            print('注文に失敗してます！成功するまで注文を出します！')
            while True:	
                time.sleep(2)
                result = self.order(side, size)
                if 'child_order_acceptance_id' in result:
                    break
        
        return result

    def getOrder(self):
        """
        現在の注文状況を取得する
        """    
        markets = self.getpositions(product_code=define._FX_BTC_JPY)
        return markets
            
    def getPositionSize(self):
        """
        現在のポジションサイズとポジション方向を確認する
        """            
        # 現在の注文状況を取得する
        markets = self.getOrder()

        # ポジションがない場合は0を返す
        if len(markets) == 0:
            return {'side':'', 'size':0}
        
        # 現在のポジションの合計を取得する
        size = 0
        for pos in markets:
            size = size + pos['size']

        position = {'side':markets[0]['side'], 'size':size}
        return position

    def checkCollateral(self, side :str):
        """
        ポジション状況を確認し条件を満たしている場合は利確する
        :return: 利確を行ったらtrue
        """
        # ポジションなしの場合は終了
        if side == define._NONE:
            return False

        try:
            # 現在の証拠金の状態を取得
            result = self.getcollateral()
        except ConnectionError:
            ('タイムアウトエラーが発生しました')
            return False

        if len(result) == 0 :
            return False

        # 利益が規定値以上ある場合、利確する
        if result['open_position_pnl'] <= (define._ROSSCUT * define._POS) or result['open_position_pnl'] >= (define._PROFIT * define._POS):
        # if result['open_position_pnl'] >= define._PROFIT * define._POS:
            pnl = result['open_position_pnl']
            confirmSide = define._SELL if side == define._BUY else define._BUY
            result = self.ordering(confirmSide, define._POS)
            print(result)
            print('含み損益' + str(pnl))
            return True

        return False
        
    def cancelPosition(self, childOrderAcceptanceId :str):
        """
        ポジション解消
        すべてのポジションを解消する
        """        

        # ポジション解消注文
        result = self.cancelallchildorders(product_code=define._FX_BTC_JPY)
        print(result)

        # 利確注文に失敗している場合、注文が通るまで繰り返す					
        if 'status' in result:
            print('注文に失敗してます！オーダーをロックしてひたすら利確注文を出します！')
            while True:	
                time.sleep(5)
                result = self.cancelallchildorders(product_code=define._FX_BTC_JPY)
                if not 'status' in result:
                    break

        # ステータスを更新する
        print('ポジションを解消しました。')

    def checkHealth(self) -> bool:
        """
        取引所の調子を取得し、注文をしてもいい状況か返す

        :return true/false
        """
        try:
            # 取引所の状態を取得
            result = self.getboardstate(product_code=define._FX_BTC_JPY)

            # 受け取った取引所の状況から注文してもいい状況かをBooleanで返す
            return utility.getTradePropriety(result['health'])
        except TypeError:
            return False

    def getDeviationRate(self) -> float:
        """
        乖離率を取得する

        :return 現在の乖離率
        """
        try:
            bf   = self.board(product_code=define._BTC_JPY)
            bfFx = self.board(product_code=define._FX_BTC_JPY)

            bfMidPrice = bf['mid_price']
            bfFXMidPrice = bfFx['mid_price']
            
            deviation = bfFXMidPrice/bfMidPrice * 100 - 100
        except TypeError:
            return 5.0            

        return deviation