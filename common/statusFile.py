from os import path
from common import define

# 注文ステータスファイル管理クラス
class statusFile():
    # コンストラクタ
    def __init__(self):
        self.path = path.dirname(path.abspath( __file__ )) + "/"

    # 現在のステータスコードを取得する
    def getStatus(self) -> str:
        f = self.__openFile('r+')
        value = f.read()
        f.close()
        return value

    # ファイルに書き込む
    def writeStatus(self, value: str) -> str:
        f = self.__openFile('w')
        f.write(str(value))
        f.close()

        return True
    
    # ファイルを開くプライベート関数
    def __openFile(self, openMode: str):
        f = open(self.path + 'position.txt', openMode)
        return f