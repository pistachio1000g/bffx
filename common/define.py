import json
from os import path

# ソースのパスを取得する
path = path.dirname(path.abspath(__file__)) + '/'

#売り買い
_BUY  = 'BUY'
_SELL = 'SELL'
_NONE = ''

# APIのURL
_API_PATH = 'https://api.bitflyer.jp'

# APIキー関係
f = open(path + 'API.json', 'r')
jsonData    = json.load(f)
_API_KEY    = jsonData['_API_KEY']
_API_SECRET = jsonData['_API_SECRET']

# マーケット
# _FX_BTC_JPY = 'BTCJPY29JUN2018'
_FX_BTC_JPY = 'FX_BTC_JPY'
_BTC_JPY    = 'BTC_JPY'
_ACTIVE     = 'ACTIVE'
_MARKET     = 'MARKET'

# httpリクエスト関係
_POST = 'POST'
_GET  = 'GET'

# ポジションの既定値
_POS     = 0.01     # 発注サイズ
_PROFIT  = 500.0   # 利確サイズ
_ROSSCUT = -350.0   # 損切りサイズ

# 発注ステータスコード
_LOCK          = 9999
_NONE_POSITION = 0
_BUY_CODE      = 1
_SELL_CODE     = -1
_BUY_NOW       = 2
_SELL_NOW      = -2

# 取引所の状態
_NOMAL      = 'NORMAL'      # 取引所は稼動しています。
_BUSY       = 'BUSY'        # 取引所に負荷がかかっている状態です。
_VERY_BUSY  = 'VERY BUSY'   # 取引所の負荷が大きい状態です。
_SUPER_BUSY = 'SUPER BUSY'  # 負荷が非常に大きい状態です。発注は失敗するか、遅れて処理される可能性があります。

# 購入判定
_VOLUME  = 150.0

# ロスカット判定
_CUT_VOLUME  = 100.0

# 価格乖離率
_DEVIATION＿RATE = 4.85

# volumeの差異
_RATIO = 3.0
_CUT_RATIO = 2.0

# 無視する取引所
ignoreList = ['bitFlyer_BTCJPY_checkbox',
              'coincheck_BTCJPY_checkbox',
              'Zaif_BTCJPY_checkbox',
              'Bitfinex_BTCUSDT_checkbox',
              'Bitstamp_BTCUSD_checkbox',
              'Gemini_BTCUSD_checkbox',
              'GDAX_BTCUSD_checkbox',
              'BitMEX_BTCUSD_checkbox',
              'HuobiPro_BTCUSDT_checkbox',
              'OKEX_BTCUSDT_checkbox',
              'OKEX_BTCUSD_WKLY_checkbox',
              'OKEX_BTCUSD_BIWKLY_checkbox',
              'OKEX_BTCUSD_QTLY_checkbox',
              'Binance_BTCUSDT_checkbox',
              'Quoinex_BTCJPY_checkbox',
              'bitFlyer_BTCJPY3M_checkbox',
              'HitBTC_BTCUSDT_checkbox',
              ]

# XRPの取引所リスト
xIgnoreList = [
              #  'Bitbank_XRPJPY_checkbox',
               'Bitfinex_XRPUSDT_checkbox',
               'Bitstamp_XRPUSD_checkbox',
               'BitMEX_XRPBTC_Z18_checkbox',
               'OKEX_XRPUSDT_checkbox',
               'OKEX_XRPUSD_WKLY_checkbox',
               'OKEX_XRPUSD_BIWKLY_checkbox',
               'OKEX_XRPUSD_QTLY_checkbox',
               'HuobiPro_XRPUSDT_checkbox',
               'Binance_XRPUSDT_checkbox',
               'Binance_XRPBTC_checkbox',
               'FCoin_XRPUSDT_checkbox',
              #  'Liquid_XRPJPY_checkbox',
               'CryptoFacilities_XRPUSD_Perp_checkbox',
               'HitBTC_XRPBTC_checkbox',
             ]