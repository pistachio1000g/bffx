import json
from datetime import datetime

from http.server import BaseHTTPRequestHandler, HTTPServer
from common import define
from common import bffFlyer
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from common import utility
import pickle
import monitor

#
# JSON受け取りクラス
#
class JsonResponseHandler(BaseHTTPRequestHandler):
    # post受け取り関数
    def do_POST(self):
        # postを受け取る
        content_len = int(self.headers.get('content-length'))
        requestBody = self.rfile.read(content_len).decode('UTF-8')
        print('requestBody=' + requestBody)

        # jsonデータに変換
        jsonData = json.loads(requestBody)
        side = utility.getRevPos(jsonData['takerSide'].upper())

        # BFラッパークラスを呼び出し
        pyp = bffFlyer.bffFlyer()

        # 現在のポジション状況を取得する
        status = utility.loadPickle()

        # メンテ時間内はトレードしない
        if utility.checkMaintenanceTime():
            if (status != define._NONE):
                print('メンテ時間に入ったので現在のポジションを決済します。')
                confirmSide = utility.getRevPos(status)
                result      = pyp.ordering(confirmSide)
                print(result)
                utility.writePickle(define._NONE)

            print('ただいまメンテの時間です。')
            self.retrunResponse()
            return

        # ポジションがない場合、新規で注文を出す
        if status == define._NONE:
            print('通知ポジション:[' + side + ']')
            result = pyp.ordering(side)
            print('新規注文を出します。')
            print(result)
            utility.writePickle(side)
            self.retrunResponse()
            return

        # 同じ方向のポジションの場合、何もしない
        print('現在のポジション' + status + '】')
        print('通知ポジション【' + side + '】')
        if status == side:
            print('同じ方向のポジションを持っているため終了。')
            self.retrunResponse()
            return
        
        # 逆方向のポジションを持っている場合、現在のポジションを解消し、新規の注文を出す
        result = pyp.ordering(side)
        result = pyp.ordering(side)
        print('新規発注[' + side + ']')
        print(result)
        utility.writePickle(side)

        # レスポンスを返して終了
        self.retrunResponse()

    # GET関数
    def do_GET(self):
        self.retrunResponse()

    # レスポンスを返すだけ
    def retrunResponse(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/json')
        self.end_headers()

# 
# メイン処理
#
def main():
    # seleniumの設定
    options = Options()
    options.binary_location = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
    options.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=options, executable_path="I:\\selenium\\chromedriver.exe")
    driver.get('https://inagoflyer.appspot.com/xrpmac')

    # サウンドを鳴らさないようにする
    for sound in driver.find_elements_by_id("sound"):
        sound.click()

    # JSONをPOSTするようにする
    for alert in driver.find_elements_by_id("sendAlertMessageCheckbox"):
        alert.click()

    # 指定したもの以外チェックを外す
    for ignore in define.xIgnoreList:
        for okex in driver.find_elements_by_id(ignore):
            okex.click()

    # 現在の注文サイドを取得する
    pyp      = bffFlyer.bffFlyer()
    position = pyp.getPositionSize()
    if position:
        side = position['side']
    else:
        side = define._NONE
    
    utility.writePickle(side)
    print('起動時の注文サイド' + side)

    server = HTTPServer(('', 8080), JsonResponseHandler)
    print('自動取引botを起動します。 起動時間:' + datetime.now().strftime("%Y/%m/%d %H:%M:%S"))
    server.serve_forever()

if __name__ == '__main__':
    main()    