import unittest
from common import bffFlyer
from common import utility
from common import define
from datetime import datetime
from common import statusFile

class testBffFlyer(unittest.TestCase):
    def testGetDeviationRate(self):
        '''
        乖離率テスト
        '''
        bff = bffFlyer.bffFlyer()

        dev = bff.getDeviationRate()
        print(dev)
        self.assertTrue(dev)

    # def testCheckConfirmVolume(self):
    #     '''
    #     利確基準チェック
    #     '''

    #     # 利確する
    #     self.assertTrue(utility.checkConfirmVolume(define._BUY_NOW, 50.0, 100.0))

    #     # 利確しない
    #     self.assertFalse(utility.checkConfirmVolume(define._BUY_NOW, 70.0, 100.0))

    #     # 利確する
    #     self.assertTrue(utility.checkConfirmVolume(define._SELL_NOW, 100.0, 50.0))

    #     # 利確しない
    #     self.assertFalse(utility.checkConfirmVolume(define._SELL_NOW, 100.0, 70.0))        

    # def testCheckOrderingVolume(self):
    #     '''
    #     新規発注基準チェック
    #     '''

    #     # 発注する
    #     self.assertTrue(utility.checkOrderingVolume(define._NONE_POSITION, 50, 100))
    #     self.assertTrue(utility.checkOrderingVolume(define._NONE_POSITION, 100, 50))

    #     # 発注しない
    #     self.assertFalse(utility.checkOrderingVolume(define._NONE_POSITION, 70, 100))
    #     self.assertFalse(utility.checkOrderingVolume(define._NONE_POSITION, 100, 70))
    #     self.assertFalse(utility.checkOrderingVolume(define._BUY_NOW, 70, 100))
    #     self.assertFalse(utility.checkOrderingVolume(define._SELL_NOW, 100, 70))

    # def testGetSideCodeByVolume(self):
    #     '''
    #     注文コード取得チェック
    #     '''
    #     side = utility.getSideStringByVolume(50, 100)
    #     self.assertTrue(side == 'SELL')

    #     side = utility.getSideStringByVolume(100, 50)
    #     self.assertTrue(side == 'BUY')

    def testGetTime(self):
        self.assertFalse(utility.checkMaintenanceTime())

    def testRevPos(self):
        '''
        getRevPos正常動作テスト
        '''
    
        self.assertEqual(define._BUY, utility.getRevPos(define._SELL))
        self.assertEqual(define._SELL, utility.getRevPos(define._BUY))

    # def testCheckConfirmVolume(self):
    #     '''
    #     利確正常動作テスト
    #     '''

    #     self.assertTrue(utility.checkConfirmVolume(define._BUY, 50.0, 75.1))
    #     self.assertTrue(utility.checkConfirmVolume(define._SELL, 75.1, 50.0))
    #     self.assertFalse(utility.checkConfirmVolume(define._BUY, 50.0, 75.0))
    #     self.assertFalse(utility.checkConfirmVolume(define._SELL, 75.0, 50.0))

    # def testcheckOrderingVolume(self):
    #     '''
    #     新規発注正常動作テスト
    #     '''
    #     self.assertTrue(utility.checkOrderingVolume(define._NONE, 350.1, 50.0))
    #     self.assertTrue(utility.checkOrderingVolume(define._NONE,  50.0, 350.1))
    #     self.assertFalse(utility.checkOrderingVolume(define._NONE, 350.0, 50.0))
    #     self.assertFalse(utility.checkOrderingVolume(define._NONE,  50.0, 350.0))
    #     self.assertFalse(utility.checkOrderingVolume(define._BUY, 350.1, 50.0))
    #     self.assertFalse(utility.checkOrderingVolume(define._BUY,  50.0, 350.1))
    #     self.assertFalse(utility.checkOrderingVolume(define._SELL, 350.1, 50.0))
    #     self.assertFalse(utility.checkOrderingVolume(define._SELL,  50.0, 350.1))

    def testStatusFile(self):
        status = statusFile.statusFile()
        status.writeStatus(define._BUY)
        status.writeStatus(define._BUY)
        status.writeStatus(define._BUY)
        side = status.getStatus()
        self.assertTrue(side == define._BUY)

        status.writeStatus(define._NONE)
        status.writeStatus(define._NONE)
        status.writeStatus(define._NONE)
        side = status.getStatus()
        self.assertTrue(side == define._NONE)

if __name__ == "__main__":
    unittest.main()
    