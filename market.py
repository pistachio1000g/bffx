import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def main():
        # CSVデータを読みこむ
        data = pd.read_csv('log/20180909201741.txt')

        # pnumpyに変換
        csvData = np.array(data) 

        # Buy-sell計算
        voldata = []
        for data in csvData:
            # if data[0] > data[1]:
            #     tempVolData = data[0] / data[1]
            # else:
            #     tempVolData = (data[1] / data[0]) * -1.0
            tempVolData = data[0] - data[1]

            voldata.append(tempVolData)

        # voldata = voldata[len(voldata)-1000:len(voldata)]
        lindata = np.copy(voldata)
        zerodata = np.full(len(lindata), 0)

        plusdata = np.full(len(lindata), 200)
        plusdata2 = np.full(len(lindata), 1000)
        
        minusdata = np.full(len(lindata), -200)
        minusdata2 = np.full(len(lindata), -1000)
        print(lindata)
        
        plt.subplot(211)
        plt.plot(voldata,color="red")

        plt.plot(zerodata,color="green")
        
        plt.plot(plusdata,color="blue")
        plt.plot(minusdata,color="blue")

        plt.plot(plusdata2,color="red")
        plt.plot(minusdata2,color="red")

        plt.subplot(212)
        plt.plot(csvData[:,3],color="blue")
        plt.show()
        
if __name__ == '__main__':
    main()