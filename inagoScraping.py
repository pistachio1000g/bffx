﻿from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from common import define
import time
from common import bffFlyer
from datetime import datetime
from common import utility

# いなごフライヤースクレイピング
def main():

    # seleniumの設定
    options = Options()
    options.binary_location = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
    options.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=options, executable_path="I:\\selenium\\chromedriver.exe")
    driver.get('https://inagoflyer.appspot.com/btcmac')

    # API定義
    pyp = bffFlyer.bffFlyer()

    # サウンドを鳴らさないようにする
    for sound in driver.find_elements_by_id("sound"):
        sound.click()

    # ビットフライヤー以外チェックを外す
    for ignore in define.ignoreList:
        for okex in driver.find_elements_by_id(ignore):
            okex.click()

    # テキストボックスの数値を変更する
    for element in driver.find_elements_by_id('bitFlyer_FXBTCJPY_alertVolume'):
        element.clear()
        element.send_keys('101')

    # 現在の注文ステータスを取得する
    position = pyp.getPositionSize()
    if position:
        side = position['side']
    else:
        side = define._NONE

    # 最初は2秒待機
    time.sleep(2)

    print('起動完了しました。')

    while True:
        # メンテ時間内はトレードしない
        if utility.checkMaintenanceTime():
            if (side != define._NONE):
                print('メンテ時間に入ったので現在のポジションを決済します。')
                confirmSide = utility.getRevPos(side)
                result      = pyp.ordering(confirmSide)
                print(result)
                side = define._NONE
            
            # 暫定的に10秒停止
            time.sleep(10)
            continue

        # 買いのボリューム取得
        for buyvol in driver.find_elements_by_id("buyVolumePerMeasurementTime"):
            buy = float(buyvol.text) + 0.00000000001

        # 売りのボリューム取得
        for sellvol in driver.find_elements_by_id("sellVolumePerMeasurementTime"):
            sell = float(sellvol.text) + 0.00000000001

        if (side != define._NONE):
            print('buy ' + str(buy) + '\n' + 'sell ' + str(sell) + '\n')

        # トレンドが反転したら利確する
        if utility.checkConfirmVolume(side, buy, sell):
            confirmSide = utility.getRevPos(side)
            result      = pyp.ordering(confirmSide)
            print('ポジションを解消しました。')
            print(result)
            side = define._NONE

        # 規定値まで損失が出ている場合は損切りする
        if pyp.checkCollateral(side):
            print(result)
            side = define._NONE

        # 取引所が重い場合はトレードしない
        if not pyp.checkHealth():
            time.sleep(1)
            continue    

        # 新規注文のサインが出たら発注を行う
        if utility.checkOrderingVolume(side, buy, sell):
            side = utility.getSideStringByVolume(buy, sell)
            print('新規注文を発注しました。')
            result = pyp.ordering(side)
            print(result)

        # 1秒待機
        time.sleep(1)

    driver.close()
    driver.quit()
    
if __name__ == '__main__':
    main()    