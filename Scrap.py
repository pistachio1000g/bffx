﻿from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from common import define
import time
from common import bffFlyer
from datetime import datetime
from common import utility
from requests.exceptions import ConnectionError

# いなごフライヤースクレイピング
def main():

    # seleniumの設定
    options = Options()
    options.binary_location = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
    options.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=options, executable_path="I:\\selenium\\chromedriver.exe")
    driver.get('https://inagoflyer.appspot.com/btcmac')

    # API定義
    pyp = bffFlyer.bffFlyer()

    # サウンドを鳴らさないようにする
    for sound in driver.find_elements_by_id("sound"):
        sound.click()

    # ビットフライヤー以外チェックを外す
    for ignore in define.ignoreList:
        for okex in driver.find_elements_by_id(ignore):
            okex.click()  

    # タイトル
    fileName = 'log/' + datetime.now().strftime("%Y%m%d%H%M%S") + '.txt'
    f = open(fileName, 'a')
    f.write('Buy,Sell,Volume,ask,bid\n')
    f.close()

    # 3秒待機
    time.sleep(3)

    # あとはひたすら繰り返す
    while True:
        # 現在の価格を取得
        try:
            market = pyp.ticker(product_code=define._FX_BTC_JPY)
        except ConnectionError:
            continue

        # 買いのボリューム取得
        for buyvol in driver.find_elements_by_id("buyVolumePerMeasurementTime"):
            buy = float(buyvol.text)

        # 売りのボリューム取得
        for sellvol in driver.find_elements_by_id("sellVolumePerMeasurementTime"):
            sell = float(sellvol.text)

        # VOLUMEの計算
        vol = buy - sell

        # ファイルに書き込み
        try:
            f = open(fileName, 'a')
            f.write("{buy},{sell},{vol},{ask},{bid}\n".format(buy = buy, sell = sell, vol = vol, ask = market['best_ask'], bid = market['best_bid']))
            f.close()
        except TypeError:
            f.close()

        # 画面に表示
        print('buy ' + str(buy))
        print('sell ' + str(sell))
        print('vol ' + str(vol))

        # 3秒待機
        time.sleep(1)

    driver.close()
    driver.quit()

if __name__ == '__main__':
    main()