from common import define
from common import utility
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

SELL_VOL = -50.0
BUY_VOL = 50.0
BUY_CUT_VOL = 0.0
SELL_CUT_VOL = -0.0

RATIO = 6.0
OVER_CUT_RATIO = 1.0

VOL = 300
CUT_VOL = 300

CUT_RATIO= 2
COU_RATIO2 = 3

CUT_PRICE = 2000.0
CONFIRM_PRICE = 2000


def main():

    # CSVデータを読みこむ
    data = pd.read_csv('log/20180830150850.txt')

    # pnumpyに変換
    csvData = np.array(data)

    code  = 0
    pos   = 0
    price = 0
    bCode = 0
    profit = []
    count = 0
    wcount = 0
    bount = 0
    pount = 0 
    for data in csvData:
        # 利確サインが出たら決済する
        if checkCutVolume(code, data[0] + 0.00000000001, data[1] + 0.00000000001) or checkConfirmVolume(code, pos, data[3], data[4]):
            pount = pount + 1
            if pount == 1:
                # soneki = round((data[4] - pos) * 0.01) if (code == define._BUY_NOW) else round((pos - data[3]) * 0.01)
                soneki = data[4] - pos if code == define._BUY_NOW else pos - data[3]
                price += soneki
                wcount = wcount + 1 if soneki >= 0 else wcount
                posPrice = data[4] if (code == define._BUY_NOW) else data[3]
                print('決済' + utility.getSideString(code * -1) +' ratio=' + str( data[0] / data[1] if (code == define._BUY_NOW) else data[1] / data[0]) + ' 価格=' + str(posPrice) + ' 損益＝' + str(soneki))
                bCode = code
                code = 0
                profit.append(price)
                count = count + 1
                pount = 0
        else:
            pount = 0

        # ポジションがなく、新規注文のサインが出たら新規注文を行う
        if checkOrderingVolume(code, data[0] + 0.00000000001, data[1] + 0.00000000001):
            bount = bount + 1
            if bount == 1:
                code = getPositionCode(data[0], data[1])
                # if code != bCode:
                pos = data[3] if (code == define._BUY_NOW) else data[4]
                print('発注' + utility.getSideString(code) +' ratio=' + str( data[0] / data[1] if (code == define._BUY_NOW) else data[1] / data[0]) + ' 価格=' + str(pos))
                bount = 0
                # else:
                #     code = 0
        else:
            bount = 0

    # 結果
    print(price * 0.01)
    print(count)
    print(wcount/count * 100)
    plt.plot(profit, color="blue")
    plt.show()

# 渡されたVolumeから利確・ロスカットを行うべきか判定する
def checkCutVolume(code :str, buy :float, sell :float) -> bool:
    '''
    True:利確する
    False:利確しなくていい
    '''
    # ポジションなし
    if code == 0:
        return False

    # 発注後、volumeの値が既定値まで行っている場合は利確・ロスカットする
    # if (code == define._SELL_NOW and buy / sell > RATIO) or (code == define._BUY_NOW and sell / buy > RATIO):
    # if (code == define._SELL_NOW and buy - sell > CUT_VOL) or (code == define._BUY_NOW and sell - buy > CUT_VOL):
    # if (code == define._BUY_NOW and buy - sell > CUT_VOL) or (code == define._SELL_NOW and sell - buy > CUT_VOL):
    if (code == define._BUY_NOW and sell / buy > OVER_CUT_RATIO) or (code == define._SELL_NOW and buy / sell > OVER_CUT_RATIO):
        return True

    # if (code == define._SELL_NOW and sell - buy > 500) or (code == define._BUY_NOW and buy - sell > 500):
    # if (code == define._BUY_NOW and sell - buy > 500) or (code == define._SELL_NOW and buy - sell > 500):
        # return True        

    return False

# volumeから発注すべきか判定する
def checkOrderingVolume(code :str, buy :float, sell :float) -> bool:
    '''
    True:発注する
    False:発注しちゃダメ
    '''
    # 既定値の範囲内にvolumeが達した時発注をかける
    # if code == 0 and ((buy - sell > VOL) or (sell - buy > VOL)):
    # if code == 0 and ((buy / sell > RATIO and sell < 30) or (sell / buy > RATIO and buy < 30)):
    if code == 0 and ((buy / sell > RATIO) or (sell / buy > RATIO)):
        return True

    return False

# 注文コード取得
def getPositionCode(buy :float, sell :float) -> int:
    # if (buy - sell > VOL):
    if (buy / sell > RATIO):
        return define._BUY_NOW
        # return define._SELL_NOW

    # if (sell - buy > VOL):
    if (sell / buy > RATIO):
        return define._SELL_NOW
        # return define._BUY_NOW

    return define._NONE_POSITION

# 渡されたVolumeから利確・ロスカットを行うべきか判定する
def checkConfirmVolume(code :str, pos :float, ask :float, bid :float) -> bool:
    '''
    True:利確する
    False:利確しなくていい
    '''
    # ポジションなし
    if code == 0:
        return False

    if code == define._BUY_NOW and pos - bid >= CUT_PRICE:
        return True

    if code == define._SELL_NOW and ask - pos >= CUT_PRICE:
        return True        

    if code == define._BUY_NOW and bid - pos >= CONFIRM_PRICE:
        return True

    if code == define._SELL_NOW and pos - ask >= CONFIRM_PRICE:
        return True

    return False

if __name__ == '__main__':
    main()